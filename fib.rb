def fibr(k)
  if k==0||k==1
    1
  else
    fibr(k-1)+fibr(k-2)
  end
end

def fibr2(k)
  f=1
  p1=1
  for i in 2..k
    p2=p1
    p1=f
    f=p1+p2
  end
  f
end
