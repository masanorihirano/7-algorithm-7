# coding: utf-8
def test1(a)
  #バブルソート
  #n^2比例
  for i in 0..a.length-2
    for j in 0..a.length-2
      if a[j]>a[j+1]
        temp=a[j]
        a[j]=a[j+1]
        a[j+1]=temp
      end
    end
  end
  return a
end

def test2(a)
  #バケツソート
  #n比例
  b=Array.new(0)
  c=Array.new(a.length)
  for i in 0..a.length-1
    if a[i]+1>b.length
      c[i]=Array.new(a[i]+1-b.length){0}
      b.concat(c[i])
    end
    b[a[i]]+=1
  end
  d=Array.new(0)
  e=Array.new(a.length)
  for j in 0..b.length-1
    e[j]=Array.new(b[j]){j}
    d.concat(e[j])
  end
  return d
end

def test3(a)
  #基数ソート
  i=0
  while
    box=Array.new(10**(i+1)){Array.new(1){}}
    active=0
    for j in 0..a.length-1
      if box[a[j]%(10**(i+1))][0].nil?
        box[a[j]%(10**(i+1))][0]=a[j]
      else
        box[a[j]%(10**(i+1))]=box[a[j]%(10**(i+1))].push(a[j])
      end
      if a[j]>=10**(i+1)
        active=1
      end
    end
    if active==0
      break
    end
    i+=1
  end
  result=Array.new(1){}
  #print box
  for k in 0..box.length-1
    if !(box[k][0].nil?)
      if result[0].nil?
        result[0]=box[k][0]
      else
        result=result.concat(box[k])
      end
    end
  end
  return result
end

def test4(a)
  #クイックソート
  b=Array.new(a.length)
  b[0]=a
  c=Array.new(a.length)
  h=0
  while
    for i in 0..a.length-1
      active=0
      if !(b[i].nil?||b[i].length==1)
        active+=1
        j=1
        k=a.length-1
        while !(j=k+2||j=k+1)
          if(b[i][j]>=b[i][0])
            if(b[i][k]<b[i][0])
              temp=b[i][j]
              b[i][j]=b[i][k]
              b[i][k]=temp
              j+=1
              k-=1
            else
              k-=1
            end
          else
            j+=1
          end
        end
        c[h]=b[i][0,j-1]
        c[h+1]=b[i][j,b[i].length-1]
        h+=2
      end
      b=c
      if active==0
        break
      end
    end
  end
  result=Array.new(1){}
  for l in 0..c.length-1
    if !(c[l].nil?)
      if result[0].nil?
        result[0]=c[l]
      else
        result=result.concat(c[l])
      end
    end
  end
  return result[0]
end
def test5(a)
  #シャッフルそーと？
  #n^nオーダー
  i=0
  while i<a.length-2
    if(a[i]>a[i+1])
      a=a.shuffle
      i=0
    else
      i+=1
    end
  end
  return a
end
