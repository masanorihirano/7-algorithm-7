# coding: utf-8

load "1106-1.rb"
load "bench.rb"

#配列作成
#配列長さ
#最大値
def testg1(l)
  m=99999
  a=Array.new(l)
  for i in 0..l-1
    a[i]=Random.rand(0..m)
  end
  test1(a)
end
def testg2(l)
  m=99999
  a=Array.new(l)
  for i in 0..l-1
    a[i]=Random.rand(0..m)
  end
  test2(a)
end
def testg3(l)
  m=99999
  a=Array.new(l)
  for i in 0..l-1
    a[i]=Random.rand(0..m)
  end
  test3(a)
end
def testg4(l)
  m=99999
  a=Array.new(l)
  for i in 0..l-1
    a[i]=Random.rand(0..m)
  end
  test4(a)
end
def testg5(l)
  m=99999
  a=Array.new(l)
  for i in 0..l-1
    a[i]=Random.rand(0..m)
  end
  test5(a)
end

#print a

#実行するプログラムを選択
#mergesort(a)
for k in 1..1000
  l=10*k
  run("testg1",l)
  run("testg2",l)
  run("testg3",l)
  run("testg4",l)
  run("testg5",l)
end
